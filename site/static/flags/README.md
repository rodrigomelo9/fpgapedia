Country flags from [www.countryflags.com](www.countryflags.com/en/image-overview).

> These high-quality images may be used free of charge for non-commercial as well as commercial purposes.
