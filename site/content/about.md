---
title: "About FPGApedia"
date: 2020-11-10
---

If you have been working with FPGAs for a while, you probably know about Xilinx (acquired by AMD in late October 2020), Intel (which bought Altera in 2015), Microsemi (who purchased Actel in 2010 and is a Microchip company since 2018) and Lattice.
Maybe, you even heard about Achronix, Efinix, Gowin and Quicklogic. Do you know Cologne Chip? Others?
What about tens of other companies, which tried to enter in the FPGA market but were purchased, out of business or disappeared?

This site aims to be a place where collect information about FPGA vendors and their devices through history.
Periodically, I will post about one or more vendors as a blog entry (see [posts](../posts/)).
Each entry will use vendors as [categories](../categories/) and devices as [tags](../tags/).

Information is collected from datasheets, publications, papers and webpages (being the [WayBack Machine](https://archive.org/web/) a big helper), distributed in the hope to be useful and interesting, but without any warranty.
Did you find something wrong, missing or which can be improved?
You can open an [issue](https://gitlab.com/rodrigomelo9/fpgapedia/-/issues) or contact me.
Contributions are welcome.
