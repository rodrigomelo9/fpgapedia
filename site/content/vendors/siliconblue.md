---
title: SiliconBlue
years: [2005, 2011]
names: ["SiliconBlue Technologies Corporation"]
sites: ["https://web.archive.org/web/*/www.siliconbluetech.com"]
country: usa
post: Lattice
---

* Founded in 2006 by a team of industry veterans, was targeting to hand-held applications with long battery life.
* They developed LUT4-based SRAM-based FPGAs with integrated Non-Volatile configuration Memory (NVM). They called its products as The mobileFPGA.
* iCE65 (65 nm) was announced in late 2007 and shipped in 2009 as iCE65L (Low Power).
* In mid 2009 a Turbo version, iCE65T (High Speed), was launched.
* In 2010 the P-series iCE65P was added, which includes a Phase-Locked Loop (PLL) to address display, SERDES, and memory interface applications.
* In 2011 the iCE40 (40 nm) were announced in two variants: the LP Series (Low Power) targeted for smartphones and the HX Series (High Performance) for tablets.
* At late 2011 SiliconBlue was acquired by Lattice, which commercialized the iCE40.
