---
title: Gowin
years: [2014]
names: ["Gowin Semiconductor Corp"]
sites: ["https://www.gowinsemi.com"]
country: china
---

* FPGA products: Little Bee (55 nm), Arora (55 nm).
