---
title: Psemi
years: [1994, 1998]
names: ["Peregrine Semiconductor Corp."]
sites: ["https://web.archive.org/web/*/http://www.peregrine-semi.com/tech/default.html"]
country: usa
---

* Psemi was founded in 1990, to develops and markets high-performance commercial integrated circuits based on its patented UTSi (ultra thin silicon) process (later known as UltraCMOS).
* In 1994 Peregrine and Xilinx signed a licensing agreement to manufacture and market its own 3V, UltraCMOS version of XC3000 FPGA family.
* There was a design demo about a 64K SRAM Xilinx 3042 FPGA (550 nm).
* Not shipped FPGA products were found and the last mention about the FPGA in the website was in 1998.
