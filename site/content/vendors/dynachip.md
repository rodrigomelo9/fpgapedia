---
title: Dynachip
years: [1993, 1999]
names: ["DynaChip Corp"]
sites: ["https://web.archive.org/web/*/www.dyna.com"]
country: usa
post: Xilinx
---

* Founded in 1993, DynaChip Corporation invented the Active Repeater Architecture for Fast FPGAs, where active elements are used to create programmable interconnects with adjacent regions and high-speed buffers connect signals to larger routing regions.
* Its FPGAs were fine-grain SRAM-based.
* In 1997 they introduced the DL5000 family (800 nm).
* 1998 was the year of DL6000 (350 nm and 250 nm for highest densities), which added SRAM blocks, and DY6000 (250 nm) which added distributed SRAM.
* In 1999, the company was focused in networking applications and announced NetFPGA DY8000 family (250 nm) in August, but in November DynaChip ceased operations.
* Xilinx acquired most of their patents and hired about 20 engineers (roughly half of the employees).
