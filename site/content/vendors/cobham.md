---
title: Cobham
years: [2014]
names: ["Cobham Limited"]
sites: ["https://www.cobhamaes.com/pagesproduct/prods-hirel-fpga.cfm"]
country: uk
prev: Aeroflex
---

* In 2014 Aerfolex was acquired by Cobham Plc. (UK), and renamed as Cobham Semiconductor Solutions.
* In 2017 a commercial version of UT6325 was added.
