---
title: Vantis
years: [1996, 1999]
names: ["Vantis Corporation"]
sites: ["https://web.archive.org/web/*/www.vantis.com/products/overview/vf1/vf1_announce.html"]
country: usa
post: Lattice
---

* Monolithic Memories Inc. (MMI) (USA).
* A semiconductor company founded in 1969, known for having invented the Programmable Array Logic (PAL).
* In 1987, it signed as the second source of Xilinx XC2000 family, which was called M2000.
* In the same year, MMI was acquired by Advanced Micro Devices Inc. (AMD).
* In 1996, the programmable logic division of AMD was separated as Vantis.


* Vantis Corporation (USA).
* Was the programmable logic company from AMD, founded in 1996.
* In 1998 the entering in the FPGAs market was announced, with the VF1 Series, a family of SRAM-based devices, with a Variable-Grain-Architecture based on LUT of 3 inputs, and manufactured in a 180 nm process.
* There is a preliminary datasheet, but the launch was delayed by software development problems and finally was never shipped.
* In 1999 AMD sold Vantis to Lattice, who discard the VF1 development.
