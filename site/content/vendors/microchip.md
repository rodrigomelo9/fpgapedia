---
title: Microchip
years: [2018]
names: ["Microchip Technology Inc"]
sites: ["https://www.microchip.com/design-centers/fpgas-and-plds"]
country: usa
prev: Microsemi
---

* Founded in 1987 when General Instruments spun off its microelectronics division as a wholly owned subsidiary. It became an independent company in 1989.
* In 2016, acquired Atmel. The last FPGA products page from Atmel appeared in the Microchip web site at the end of 2017. The Rad-hard FPGAs page also appeared, but without products and it dissapeared from the Rad-Hard section in 2018.
* In 2018, acquired Microsemi. At the time to write this, no info about what will happen with FPGAs from Microsemi is known.

* www.microchip.com/design-centers/programmable-logic/field-programmable-gate-array (Jun, 2018)
