---
title: QuickLogic
years: [1988]
names: ["Quicklogic Corporation"]
sites: ["https://www.quicklogic.com"]
country: usa
---

* Founded in 1988 by the engineers who invented the PAL device and PALASM software.
* In 1991 they introduced FPGAs.
* 2003 end general fpga

* Also has eFPGA (ArcticPro)
* Core: ArcticPro
* GLOBALFOUNDRIES, SMIC (Semiconductor Manufacturing International Corporation)

* https://www.quicklogic.com/company/quicklogic-history/
* https://semiwiki.com/x-subscriber/quicklogic/3588-a-brief-history-of-quicklogic/
