---
title: "Flex Logix"
years: [2014]
names: ["Flex Logix Technologies, Inc"]
sites: ["https://www.flex-logix.com"]
country: usa
---

* Core: EFLX
