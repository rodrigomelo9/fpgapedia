---
title: SEi
years: [1998, 2001]
names: ["Space Electronics Inc."]
sites: ["https://web.archive.org/web/*/http://www.spaceelectronics.com/products/_space/catalog/logic_up.html"]
country: usa
---

* SEi was founded in 1992.
* SEi utilized commercial-off-the-shelf IC chips in die form then protected these parts from the damaging effects of the natural radiation in space (radiation-hardened). One of them seems to was the GF100K ProASIC family: the website in 1996 said "FPGA up to 50,000 gates Latchup Protected" and there is a paper in 1997 about this topic where a GF10009 was used. SEI's device was based on a 650 nm CMOS antifuse process and was available with densities ranging from 2000 to 9000 gates.
* In 1998 Actel joined with SEi to combine Actel's antifuse-based FPGAs with SEi's RAD-PAK package shielding technology.
* Preliminary RAD-PAK FPGA datasheets were offered in Actel's website in 1999, but in year 2000 an advice appeared about that RAD-PAK products are no longer available through Actel and SEi must be contacted for data sheets and other information.
* The devices were RP1280A (based on A1280A ACT2) and RP14100A (based on A14100A ACT3).
* Actel outsources RAD-PAK FPGA sales and marketing to Space Electronics (Oct, 1999).
* In 1999 SEi was acquired by Maxwell Technologies Inc.
* There is a preliminary datasheet branded as SEi (very similar to the previous by Actel) in the SEi website (2001).
* The SEi name was used until 2002, and all the references about FPGAs disappeared.

* www.spaceelectronics.com/SpaceProd/Technologies/rad_pak.html (May, 1999)
* www.actel.com/products/radhard.html (Jan, 1999)
* Single event latchup protection of integrated circuits, P. Layton, D. Czajkowski, 1. Marshall, H. Anthony, R. Boss. 1997.
* https://web.archive.org/web/20010905221904/http://www.spaceelectronics.com/products/_space/catalog/logic_up.html (Sep. 2001).
