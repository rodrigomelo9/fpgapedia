---
title: CSwitch
years: [2003, 2009]
names: ["CSwitch Corp"]
sites: ["https://web.archive.org/web/*/www.cswitch.com"]
country: usa
post: Agate
---

* Founded in late 2003, with focus on communication networks.
* In 2006 the company unveiled a new architecture called Configurable Switch Array (CSA), claimed to be capable of narrowing the performance and density gaps between FPGAs and ASICs.
* The CS90 devices were shipped in 2008, a LUT-based SRAM-based FPGA (90 nm). It includes memory blocks and SERDES.
* In 2009, the company halted operations and entered in negotiations with potential buyers. CSwitch was bougth and merged with Agate Logic (USA).
