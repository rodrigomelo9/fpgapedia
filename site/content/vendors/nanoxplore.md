---
title: NanoXplore
years: [2010]
names: [NanoXplore]
sites: ["http://www.nanoxplore.com"]
country: france
---

* Founded in 2010 by former employees of Abound logic.
* Core: NX-eFPGA
