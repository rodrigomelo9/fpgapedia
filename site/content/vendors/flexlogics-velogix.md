---
title: "Flexlogics - Velogix"
years: [2002, 2009]
names: ["Flexlogics Inc", "Velogix Inc"]
sites: ["https://web.archive.org/web/*/www.flexlogics.com", "https://web.archive.org/web/*/www.velogix.com"]
country: usa
---

* Founded in 2002 (USA) as Flexlogics Inc, which mission was to become a leading provider of high-performance, high capacity, programmable ICs and design software.
* In 2005 was renamed to Velogix Inc and until half of 2006 worked in stealth mode.
* A paper in 2006 despicted that the architecture was based in LUTs.
* A paper in 2007 remarks that it was called Vx200, LUT-based, manufactured in 90 nm. Was probably SRAM-based, because is compared against FPGAs using this type of interconnection and due the declared performance.
* There is not much more info since 2007 and in 2009 Velogix Inc. ran out of funds and went out of business.

* Timing-Driven Placement for Heterogeneous Field Programmable Gate Array, Bo Hu (Velogix Inc). ICCAD'06. 2006.
* A 180 GFLOP/s, 15 GFLOP/W, 500 million transistor FPGA in 90nm CMOS, Ashok Vittal, Hare K. Verma. 2007
