---
title: Intel
years: [2015]
names: ["Intel Corporation"]
sites: ["https://www.intel.com/content/www/us/en/products/programmable.html"]
country: usa
prev: Altera
---

* Is an American multinational corporation and technology company headquartered in Silicon Valley, founded in 1968.
* iFX740/80
* In 2015 acquired Altera, which was the main Xilinx competitor for years, and incorporate FPGAs to its portfolio.
* Current FPGAs products are Statrix, Arria, Cyclone and MAX. Some versions of the first three includes a SoC (FPGA+SoC).
* Xenon?

* Altera licensed its programmable logic technology to Intel in 1984. Ten years later, Altera acquired Intel’s PLD and FPGA business for about $50 million in cash and stock.
