---
title: Tabula
years: [2003, 2015]
names: ["Tabula"]
sites: ["https://web.archive.org/web/*/www.tabula.com"]
country: usa
---

* Was a fabless semiconductor company based in silicon Valley, founded in 2003.
