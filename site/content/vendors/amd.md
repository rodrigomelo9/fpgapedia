---
title: AMD
years: [2020]
names: ["Advanced Micro Devices, Inc."]
sites: ["https://www.amd.com/en/corporate/xilinx-acquisition?utm_campaign=xilinx-acquisition&utm_medium=redirect&utm_source=301"]
country: usa
prev: Xilinx
---
