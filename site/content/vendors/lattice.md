---
title: Lattice
years: [1983]
names: ["Lattice Semiconductor Corporation"]
sites: ["http://www.latticesemi.com"]
country: usa
prev: Agere
---

* Is an American manufacturer of high-performance PLDs headquartered in Oregon, founded in 1983.
* Is considered the inventor of the GAL (1985).
* At the end of 2001, Lattice acquired Agere's FPGA business (ORCA).
* In 2011 acquired SiliconBlue and incorporate iCE to its FPGA products.
* Current FPGAs products are ECP, MachX0 and iCE.
