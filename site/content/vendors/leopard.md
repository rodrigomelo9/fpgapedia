---
title: "Leopard Logic"
years: [2002, 2005]
names: ["Leopard Logic Inc"]
sites: ["https://web.archive.org/web/*/www.leopardlogic.com"]
country: usa
post: Agate
---

* LLI was founded in 2000. They announced general purpose embedded FPGA core for the 180 nm CMOS process. Is called CCL18 series (Customer Configurable Logic block), LUT4-based SRAM-based.
* In 2002 HyperBlox was presented (probably the CCL18 renamed), in two variants: FP (Field Programmable) and MP (Mask Programmable), where the SRAM configuration is replaced with a single-layer via-mask configuration. Were offered in 180 nm and 130 nm.
* In 2004 Gladiator CLD (Configurable Logic Device) series was announced. Combines FPGA technology with hardwired ASIC logic. The basic building blocks of are the HyperBlox FP and MP fabrics, which are combined with memories, Multiply-Accumulate (MACs) and flexible high-speed I/Os.
* The first (and only) member of the Gladiator CLD family was the CLD6400, manufactured in 130 nm.
* In 2005, Leopard Logic ceased operations. All its technology, patents and other intellectual property have been sold or transferred (Agate Logic USA).
