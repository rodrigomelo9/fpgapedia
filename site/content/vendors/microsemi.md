---
title: Microsemi
years: [2010, 2018]
names: ["Microsemi Corporation"]
sites: ["https://www.microsemi.com/product-directory/1636-fpga-soc"]
country: usa
prev: Actel
post: Microchip
---

* Is an American manufacturer of semiconductor headquartered in Silicon Valley, founded in 1959.
* In 2010 acquired Actel and incorporate FPGAs to its portfolio.
* Current FPGAs products are SmartFusion (FPGA+SoC), Igloo, RTG4 and PolarFire.
