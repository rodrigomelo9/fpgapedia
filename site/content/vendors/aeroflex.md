---
title: Aeroflex
years: [2000, 2014]
names: ["Aeroflex Inc"]
sites: ["https://web.archive.org/web/*/ams.aeroflex.com/ProductPages/RH_fpga.cfm"]
country: usa
post: Cobham
---

* QuickLogic announced in year 2000 that it has licensed its FPGA technology to Aeroflex UTMC (subsidiary of Aeroflex Incorporated, formed in 1999 when UTMC Microelectronic Systems was acquired), for the development and sale of radiation-hardened integrated circuits for space applications.
* In 2004, as Aeroflex, RadHard Eclipse Family were announced, Fine-grain SRAM-based FPGAs (250 nm), for aerospace and defense applications.
* Preliminary, UT6250 and UT6325 devices were announced, but only UT6325 was commercialized.
* In 2014, UT6325 was still offered, and Aeroflex was acquired by the aerospace company Cobham.
