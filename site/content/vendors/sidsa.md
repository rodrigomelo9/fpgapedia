---
title: Sidsa
years: [1996, 2000]
names: ["Semiconductores Investigación y Diseño SA"]
sites: ["https://web.archive.org/web/20000818001211/http://www.sidsa.es/fipsoc.htm"]
country: spain
---

* The FIPSOC (FIeld Programmable System On Chip) was a project financed by the FP4-ESPRIT 4 programme of the European Commission, between 1996 and 1998. It involved several European Institution, mainly Spanish.
* SIDSA (Semiconductores Investigación y Diseño SA), which was founded in 1992, was the project coordinator.
* FIPSOC was a family of general-purpose field programmable integrated circuits including a FPGA (LUT4-based SRAM-based, manufactured in 500 nm), a FPAA and a microcontroller (8051).
* Last references about FIPSOC in the SIDSA's website are dated in 2001.
