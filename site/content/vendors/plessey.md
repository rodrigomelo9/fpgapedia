---
title: Plessey
years: [1989, 1992]
names: ["Plessey Semiconductors Ltd"]
country: uk
---

* In 1989, the ERAs (Electrically Reconfigurable Arrays) were introduced, a fine-gran SRAM based FPGA, licensed from Pilkington, with plans to be manufactured using 1400 and 1000 nm.
* The same year, Plessey Company Plc was acquired by GEC (General Electric Company, also from UK).
* Some papers and books cited a preliminary datasheet about ERA60100 Series from Pelssey (1989) and a datasheet from GEC Pelssey (1991), but were not found. There are also mentions about the ERA70000 Series in a 1992 article. No more related data was found.
* In 2010 was Plessey Semiconductors was relaunched.

* ELECTRICALLY RECONFIGURABLE ARRAYS - ERAS, PAUL DILLIEN, 1989.
* Plessey (1989). ERA60100 Preliminary Datasheet. Plessey Semiconductor, Swindon, England.
* GEC Plessey Semiconductors, ERA60100 Electrically Reconfigurable Array Data Sheet, GEC Plessey Semiconductors Ltd., Swindon, Wiltshire SN2 2QW, UK, 1991
* User configurable Logic, Patrick W. Foulk. Computing & Control Engineering Journal, 1992.
* www.plesseysemiconductors.com (2010)
