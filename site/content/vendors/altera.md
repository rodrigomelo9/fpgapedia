---
title: Altera
years: [1983, 2015]
names: ["Altera Corporation"]
sites: ["https://web.archive.org/web/*/www.altera.com"]
country: usa
post: Intel
---

* Is considered the inventor of the CPLD (1984).
