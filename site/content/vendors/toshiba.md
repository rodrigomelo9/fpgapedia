---
title: Toshiba
years: [1989, 1992]
names: ["Toshiba Corporation"]
country: japan
---

* In 1989, Toshiba and Pilkington Micro-electronics signed a technical collaboration agreement.
* In 1991 a fine-grain SRAM-based FPGA, manufactured in 800 nm, was announced. It uses the DPLD technology from Pilkington.
* The plans were mass production in 1992, but not more data about products was found.

* A large Scale FPGA with 10K Core Cells with CMOS 0.8um 3-Layered Metal Process, H.Muroga, H.Murata, Y .Saeki, T.Hibi, Y .Ohashi, T.Noguchi, and T.Nishimura. Toshiba Corporation. Proceedings of the IEEE 1991 Custom Integrated Circuits Conference.
