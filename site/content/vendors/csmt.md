---
title: CSMT
years: [2000, 2017]
names: ["Chengdu Sino Microelectronics Technology Co Ltd"]
sites: ["https://web.archive.org/web/*/www.csmsc.com"]
country: china
---
