---
title: Actel
years: [1985, 2010]
names: ["Actel Corporation"]
sites: ["https://web.archive.org/web/*/www.actel.com"]
country: usa
post: Microsemi
---

* TI was a licensed second source of Actel products since 1988. In 1995 Actel acquires TI FPGA business.
* Matsushita is marketing Actel's FPGAs in Japan and is also acting as a foundry for the U.S. company's FPGA products. In 1994, the partners expanded their relationship to include the joint development of advanced semiconductor process technologies.
* VariCore (varicore.actel.com/cgi-bin/varicore) eFPGA.
* The VariCore embedded programmable gate array (EPGA) was developed by ProSys Inc., which Actel acquired in year 2000.
* Acquired by Microsemi in 2010.

* Actel Corporation is the world's leading supplier of antifuse- based field programmable gate arrays (FPGAs) and associated software development tools.
* Actel Corporation entered the programmable logic market in 1988 with the shipment of its firs FPGA
* 1996: The company's products now include the ACT1, ACT2, 1200XL, and ACT3 logic families.
* 1996: ACT 1, ACT 2, Accelerator (ACT 3) families, and the Integrator (1200XL and 3200DX).

GateField developed the flash-based FPGA architecture used in Actel's ProASIC product line. Actel had an equity stake in GateField and purchased the rest of the company on May 11. GateField now is adapting its technology to produce a flash-based FPGA core, which Actel wants because of the lower die size of flash compared with SRAMs.
