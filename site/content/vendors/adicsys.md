---
title: ADICSYS
years: [2010]
names: [ADICSYS]
sites: ["https://www.adicsys.com"]
country: france
---

* Advanced Integrated Circuit Systems
* Founded in 2010 by former employees of Abound logic.
* Core: SPC (Synthesizable Programmable Core)
