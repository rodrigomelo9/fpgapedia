---
title: Gatefield
years: [1993, 1998]
names: ["Gatefield Corp"]
sites: ["https://web.archive.org/web/*/www.gatefield.com"]
country: usa
post: Actel
---

* Formed in 1993 as GateField division of Zycad Corp., a company from California which changed their name to GateField Corp. in 1997 to reflect their new ProASIC (Programmable ASIC) business thrust.
* In 1995 the first ProASIC GF100K member was shipped, after two year of cooperation with ROHM (recognized for its expertise in flash technology). It is a fine-grain flash-based FPGA, using 800 nm.
* In 1997 the families GF200F, GF250F and GF260F were released.
* The GF200F family, with an improved architecture, increased performance and gate utilization, using 600 nm.
* The GF250F family provides increased performance and total gate availability, while the GF260F family added embedded memory.
* In 1998 Actel paid for exclusive rights to market and sell ProASIC devices manufactured at 0.25-micron and below. After supporting the operations of GateField for two years, Actel acquired the flash-memory-based architecture.
