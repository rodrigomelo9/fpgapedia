---
title: Cypress
years: [1992, 1997]
names: ["Cypress Semiconductor Corporation"]
sites: ["https://web.archive.org/web/19980222112743/http://www.cypress.com/products/"]
country: usa
---

* Founded in 1982.
* FPGA (Quicklogic) 1992-1997.
* Second source of Quicklogic (antifuse).
* pASIC and Ultra38000 (37000 and 39000 were CPLDs).
