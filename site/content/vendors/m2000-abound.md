---
title: "M2000 - Abound Logic"
years: [1996, 2010]
names: ["M2000", "Abound Logic, Inc"]
sites: ["https://web.archive.org/web/*/www.m2000.fr", "https://web.archive.org/web/*/www.aboundlogic.com"]
country: france
---

* The company was founded as M2000 in 1996, but the founders had previously started Meta Systems, which was acquired by Mentor Graphics in 1996, where they developed a emulation system based on custom FPGAs. That were the basis for the Abound Logic product.
* The first years, M2000 offered FlexEOS reprogrammable embedded array, LUT4-based SRAM-based eFPGA. Samples have been produced in 130 and 90 nm.
* In 2007 M2000 announced a breakthrough programmable logic technology.
* The company changed its name to Abound Logic in 2009, and presented its Raptor family of FPGAs, which appears to be the breakthrough technology. It was manufactured in 65 nm and includes memory blocks, DSPs and SerDes.
* In 2010 Abound Logic reported to have shut down.
