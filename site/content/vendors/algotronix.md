---
title: Algotronix
years: [1989, 1993]
names: ["Algotronix Ltd"]
sites: ["http://www.algotronix.com/company/Photo%20Album.html"]
country: uk
post: Xilinx
---

* Algotronix Ltd. (UK). Algorithms in Electronics.
* In 1985 research work begin, at Edinburgh University, on CAL (Configurable Logic Array) architecture.
* In 1988 the CAL256 chip was the first SRAM programmed member of the CAL family, manufactured in 2um.
* Algotronix is formed in 1989, and work starts on the CAL1024, using 1.5um. It was the first FPGA to offer random access to its control memory and to provide signal sharing on I/O's to facilitate building arrays of devices.
* In 1993 the architecture for a next generation is defined. Algotronix was acquired by Xilinx and its technology forms the basis of the XC6200 family.
* In 1998 the Algotronix name was bought from Xilinx and was reformed as an independent consulting company.
