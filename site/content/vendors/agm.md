---
title: AGM
years: [2012]
names: ["AGM Microelectronics"]
sites: ["http://www.alta-gate.com"]
country: china
---

* A fabless company engaged in low-power Programmable chip design.
