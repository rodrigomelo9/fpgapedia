---
title: MRC
years: [1997, 2004]
names: ["MRC Microelectronics"]
sites: ["https://web.archive.org/web/*/www.mrcmicroe.com"]
country: usa
---

* Established in 1982, also knows as MRC Albuquerque, was a division of Mission Research Corporation with capabilities in designing radiation-hardened ICs.
* In year 2000 they presented a paper about a new FPGA architecture (LUT4-based) for total dose tolerance and SEU immunity in space environments, a project started in 1997.
* The same year two RadHard FPGAs were announced: Orion-4K (EEPROM, 800 nm) and Orion-16 (SRAM and EEPROM versions, 250 nm), which would be available in Q4 2000 and Q4 2001 respectively.
* During 2002 there was a reference about a future 64K SRAM FPGA (180 nm) to be introduced in 2003, but no more info was found.
* MRC was acquired by ATK (Aliant Techsystems) in 2004, and no info was found about more work in FPGAs.

* A Reconfigurable, Nonvolatile, Radiation Hardened Field Programmable Gate Array (FPGA) for Space Applications, David Mavis, Bill Cox, Dennis Adams, and Richard Greene. 2000.
