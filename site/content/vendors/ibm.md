---
title: IBM
years: [1993, 1996]
names: ["IBM Microelectronics"]
country: usa
---

* IBM announced its entrance into the programmable logic market in 1993.
* A paper in 1996 presented a SRAM-based FPGA architecture using the licensed AT6000 architecture. The first chip of the IBM10000 family was called IBM10016
* The same year, IBM announced the termination of its FPGA development and production program.

* An SRAM-Based FPGA Architecture, Scott Gould, Brian Worth, Kim Clinton, Eric Millham, Fran Keyser, Ron Palmer, Steve Hartman, Terry Zittritsch
IBM Microelectronic. Custom Integrated Circuits Conference, 1996.
