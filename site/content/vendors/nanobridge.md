---
title: NanoBridge
years: [2019]
names: ["NanoBridge Semiconductors, Inc."]
sites: ["https://nanobridgesemi.com"]
country: japan
---

# NEC

* NEC Corporation (Japan)
* In 2015 announced the development of NB-FPGA.
* NB-FPGA is a LUT4-based NanoBridge-based FPGA. NanoBridge is a interconnect technology which supports high temperature, is non-volatile and RadTolerant.
* In 2017 was announced the participation of AIST (National Institute of Advanced Industrial Science and Technology of Japan).
* There is a NEC's paper in 2017 were a 65 nm chip was manufactured and tested.

* NanoBridge-Based FPGA in High-Temperature Environments. 2017.
* https://www.nec.com/en/press/201901/global_20190118_01.html (Jan, 2019)

# NanoBridge Semiconductors, Inc. (nbs)

* https://nanobridgesemi.com/?page_id=170
* https://global.jaxa.jp/activity/pr/jaxas/no079/05.html
* https://www.design-reuse.com/news/41568/nec-radiation-tolerant-fpga.html
* https://www.researchgate.net/publication/330308771_Reducing_the_power_consumption_and_increasing_the_performance_of_IoT_devices_by_using_Nano-Bridge-FPGA
