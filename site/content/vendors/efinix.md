---
title: Efinix
years: [2012]
names: ["Efinix Inc"]
sites: ["https://www.efinixinc.com"]
country: usa
---

* Santa Clara, Calif.
* Startup backed by Xilinx and Samsung
* Received samples of its Quantum programmable ICs from Semiconductor Manufacturing International Corp. (SMIC).
