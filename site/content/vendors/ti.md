---
title: TI
years: [1988, 1995]
names: ["Texas Instruments Inc"]
country: usa
post: Actel
---

* TI was a licensed second source of Actel products since 1988.
* Their FPGAs were called TPC and were based on fine-grain antifuse technology.
* The TPC10 Series (1989) had 4 members, two manufactured in 1200 nm and the other two in 1000 nm.
* The TPC12 Series (1991) had 3 members, two manufactured in 1200 nm and one in 1000 nm.
* There was a product preview about TPC14 Series (1993) which would to be manufactured in 800 nm, but seems that was not marketed.
* The TPC10 devices used combinatorial logic modules, while the TPC12 and TPC14 devices used combinatorial and a sequential logic modules.
* In 1995 Actel acquires TI FPGA business.
