---
title: Achronix
years: [2004]
names: ["Achronix Semiconductor Corporation"]
sites: ["https://www.achronix.com"]
country: usa
---

* Is a fabless semiconductor.
* FPGA products: Speedster (2008), Speedster22i (2012). Also has eFPGA Speedcore (2016).
* Core: Speedcore
* TSMC
