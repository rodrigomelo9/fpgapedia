---
title: National
years: [1992, 1997]
names: ["National Semicounductors"]
country: usa
---

* In 1992, National Semiconductors (NSC) and Concurrent Logic co-developed the CLAy (Configurable Logic Array) FPGAs, based in the CLi6000 architecture.
* The CLAy 10 (CLi6002) and CLAy 31 (CLi6005) are fine-grain SRAM-based FPGAs (800 nm).
* NSC originally planned to offer them commercially, but ultimately decided not to offer them as products.
* The CLAy 31 technology was also packaged in a Field Configurable Multi Chip Module (FCMCM), arranged in a 2 x 2 array.
* The development group was shutted down in 1997.

* High Level Compilation for Fine Grained FPGAs, Maya Goltl-iale, Edson Gomersall. 1993.
* National Semiconductor, "Configurable Logic Array (CLAy) Data Sheet", 1993
