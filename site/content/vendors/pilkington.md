---
title: Pilkington
years: [1986, 1997]
names: ["Pilkington Micro-electronics Ltd"]
sites: ["https://web.archive.org/web/*/www.pmel.com/digital.html"]
country: uk
post: Motorola
---

* Pmel was founded in 1986, as a subsidiary of Pilkington Plc. (a glass company), to develop advanced programmable and reconfigurable microelectronic, to be licensed to ICs manufacturers.
* Pmel licensed its DPLD (Dynamically Programmable Logic Device), a fine-grain SRAM-based FPGA, to Plessey (1989) and Toshiba (1991). There is a paper from 1994 which speak about a device from Pmel called generically as DPLD 3K6, but no more relevant information about that was found.
* In 1994, a new fine-grain SRAM based architecture, called TS1, was presented.
* In 1996, the web page of Pmel descrived the TS Series, which included TS1 and TS2 families. The TS1 series FPGAs were available under license as the Motorola MPA1000 series. The TS2 Series would be available for licensing in Q2 1996.
* In 1997, Pmel was acquired by Motorola.

* Design Experience with Fine-grained FPGAs, P. Lysaght, D. McConnell and H. Dick. FPL 1994.
* An effective hardware/software solution for fine grained architectures, Darren M. Wedgwood. 1994.
