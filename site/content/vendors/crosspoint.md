---
title: Crosspoint
years: [1989, 1996]
names: ["Crosspoint Solutions, Inc"]
country: usa
---


* A paper presented in 1992 introduced a MPGA (Mask Programmable Gate Arrays) compatible (use same tools and libraries as ASIC) FPGA. A fine-grain Antifuse FPGA (800 nm) called CP20K. A first chip would be available at the end of 1991 (4K2 gates) and two more (8K4 and 12K gates) in 1991. However, it seems that something delayed the production because were again announced in 1995.
* There is a datasheet (Feb, 1993) about CP20K CrossPoint FPGAs from NEC. Nothins is said in NEC site (necel.com) in 1999.
* Also in 1995, a new advanced super-fine grained architecture was unveils, the CP100K CrossFire (up to 100K gates), using 500 nm (with plans of migration to a 0.25-micron technology resulting in chips at 250K and beyond gates).
* In 1996, Crosspoint announced a new relationship with LG Semicon Co., to provide as second source, manufacturing capacity in 800 and 600 nm. LG would receive license rights to develop and market products based on CP20K FPGA technology. No more info about LG products were found to be considered as a FPGA vendors.
* Last news were about the CoreBank program to use with CP20K and CP100K CrossFire FPGAs (1996), and after that the company disappeared.

* An MPGA Compatible FPGA Architecture, David Marple and Larry Cooke,Crosspoint Solutions, Inc. Custom Integrated Circuits Conference, 1992.
* An MPGA-like FPGA, DAVID MARPLE, Crosspoint Solutions, Inc. IEEE Design & Test of Computers, 1992.
