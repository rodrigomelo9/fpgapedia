---
title: "AT&T - Lucent - Agere"
years: [1992, 2001]
names: ["AT&T Microelectronics", "Lucent Technologies, Inc", "Agere System Inc."]
sites: ["", "https://web.archive.org/web/*/www.lucent.com/micro/fpga/hardware.html", "https://web.archive.org/web/*/www.agere.com/netcom/orca"]
country: usa
post: Lattice
---

* AT&T Microelectronics (USA), formed in 1988 as part of AT&T Technologies Inc., was the second source of Xilinx XC3100 family (1992), which was called ATT3000.
* In 1993 its own FPGA SRAM-based was presented, the ORCA (Optimized Reconfigurable Cell Array) 1C series, based in LUT of 4 inputs and manufactured in a process of 600 nm.
* One year later, ORCA 2C was introduced, which is an enhanced of the 1C series manufactured in 500 nm. Also a low voltage version (3.3v), called ORCA 2T, was introduced.
* Finally, in 1996, ORCA 2CA and 2TA were presented, which are based on 2C/T architecture, with some performance and functionality improves and migrated to 350 nm.
* In 1996, it became the Microelectronics division of Lucent Technologies.


* Lucent Technologies, Inc. (USA).
* In 1996 AT&T Technologies became Lucent Technologies.
* As Lucent Microelectronics, it commercialized ATT3000 and Orca 2CA and 2TA series.
* In 1997 the ORCA 3C and 3T series was built based on ORCA 2 architecture, with enhancements and innovations, also manufactured in a 350 nm process.
* A year latter, a version of ORCA 3T with an embedded PCI interface was presented, and called ORCA 3TP (where P is for plus).
* Very low voltage versions (2.5v), ORCA 3L and 3LP where launched in 1999.
* A Preliminary Data Sheet of the year 2000 exists about ORCA 4E Series (130 nm), but was not commercialized by Lucent.
* In 2001, the Microelectronics Group was renamed Agere Systems, taking the name of a company which Lucent acquired.


* Agere System Inc. (USA).
* Formerly the Microelectronics division of Lucent Technologies, it focused their business in communication components.
* There is a Product brief of the year 2001 about ORCA 4E Series, but the FPGA business of Agere was acquired by Lattice at the end of the same year.
* In 2007 Agere System and LIS Logic were merged in LSI Corporation.
