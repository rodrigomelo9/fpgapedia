---
title: "Viciciv - Tier Logic"
years: [2003, 2010]
names: ["Viciciv Technology", "Tier Logic Inc"]
sites: ["https://web.archive.org/web/*/www.viciciv.com", "https://web.archive.org/web/*/www.tierlogic.com"]
country: usa
---

* Tier Logic (formerly Viciciv Technology Inc) was founded in 2003, with focus on provide higher-capacity chips for a lower cost.
* There were in stealth mode until March of 2010, when they offered the first details on the Series A of its three-dimensional technology.
* In classical FPGAs most of the die area is taken by the wires and the interconnect switches and muxes. Their architecture has separated user and configuration circuits into 3D stacked layers.
* They offered a platform to easily migrate FPGA designs into ASICs (TierFPGA to TierASIC). They only needed to replace the interconnect configuration SRAM cells at the top with metal.
* Their TierFPGA was LUT-based SRAM-based, manufactured in 90 nm.
* In July 2010 they shut down operations, unable to find funding for a series B.
