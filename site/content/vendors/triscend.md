---
title: Triscend
years: [1997, 2004]
names: ["Triscend Corporation"]
sites: ["https://web.archive.org/web/*/www.triscend.com"]
country: usa
post: Xilinx
---

* Founded in 1997.
* Their products were considerated Configurable System-on-Chip (CSoC), where the programmable logic part was LUT4-based SRAM-based.
* In 1999, E5 was presented, which includes a performance-enhanced Accelerated 8051 embedded microcontroller, blocks of SRAM and programmable logic. It was manufactured in 350 nm.
* A7 was presented in year 2000. Uses the same architecture that E5, but instead of a 8 bit 8051, a 32 bit ARM7 was used. Also some peripherals such as timers and UARTs were added.
* A7V was announced in 2003. It is an A7 (which was also known as A7S) optimized for use in complex electronic motor drives (more peripherals were added).
* Acquired by Xilinx in 2004. Products and software were discontinued.
