---
title: Xilinx
years: [1984, 2020]
names: ["Xilinx, Inc"]
sites: ["https://www.xilinx.com"]
country: usa
post: AMD
---

* Is an American company founded in Silicon Valley in 1984.
* Is one of the mayor manufacturers, known for inventing the FPGA and as the first semiconductor company with a fabless manufacturing model.
* LCA (Logic Cell Array).
* Current FPGAs products are Virtex, Kintex, Artix, Spartan and Zynq (FPGA+SoC).
* One of the two main FPGA providers.
* Inventor of the FPGA.
* First semiconductor company with a fabless manufacturing model.
* Xilinx started out as an FPGA company first and then later acquired the likes of Plus Logic and the Coolrunner family from Philips Semiconductors to complement their FPGA line with CPLD products.
* In 1993 Algotronix was acquired and its technology was used as the base of the XC6200 FPGA product family. It was very popular with the research community but was not successful commercially. In 1998 Xilinx discontinued the XC6200.
* 1996: Xilinx has a second-source agreement with Harris Semiconductor for rad-hard FPGAs.
* In connection with the settlement of patent  litigation in 1993,  Actel granted Xilinx a license under certain of Actel's patents that permits Xilinx to manufacture and market antifuse-based products. Xilinx announced in 1996 that it had discontinued its antifuse-based FPGA product line.
