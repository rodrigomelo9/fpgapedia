---
title: "Agate-CME"
years: [2005, 2017]
names: ["Agate Logic", "Capital Microelectronics Co. Ltd."]
sites: ["https://web.archive.org/web/*/www.agatelogic.com.cn/zhichi/file8.html", "https://web.archive.org/web/*/www.capital-micro.com"]
country: china
---

* Agate Logic Inc. (China)
* In 2005 Agate Logic USA (also known as Cloudchip) and Beijing were founded.
* Agate Logic USA bought Leopard Logic's IPs in 2005.
* Agate Logic USA acquired IP assets of CSwitch Corp. in 2009. From there, is seems that the company is in stealth mode (since 2015, its new website argue that), but still active, because their last patent is dated in 2017.
* Agate Logic Beijin was the first China corporation with a self-developed FPGA architecture.
* In 2007, AG1F1 was released, the first product of FP-MCU series. It was a Configurable System-on-Chip (CSoC) device optimized for embedded systems applications, which integrates in a single chip a FP (Field Programmable) core, dual-port memory (DPRAM) and a high-performance 8051 MCU.
* In 2009 Angelo was released, a LUT-based SRAM-based FPGA (130 nm). Also Astro was released, a CSoC device which seems an improved version of FP-MCU series.
* In 2010 Astro II was released.
* E-Town Capital and Agate Logic Beijing jointly founded Capital Microelectronics (CME) in 2011 (Agate Logic changed to CME).

* www.agatelogic.com.cn/zhichi/file8.html (Apr, 2010)
* www.agatelogic.com (Jun, 2018)


* Capital Microelectronics Co. Ltd. (China)
* Formely Agate Logic Beijing.
* Astro and Astro II were renamed to CME-M0 and M1.
* In 2012 CME-M5 was released. It had three series (C: FPGA + MCU with SRAM; R: FPGA + SRAM; P: FPGA).
* In 2014 CME-M7 and CME-HR were released. In CME-M7 the MCU was changed to ARM Cortex-M3. CME-HR is a LUT-based SRAM-based (40 nm) FPGA, targeted for ultra low power applications like mobiles.
* In 2016 CME-C1 was announced. It used LUT6 and was manufactured in 40 nm. Also Serdes, DDR controller and PCI hardcores were added.

* www.capital-micro.com (Feb, 2012)
* www.capital-micro.com/en (Jun, 2018)
