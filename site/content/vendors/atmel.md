---
title: Atmel
years: [1993, 2016]
names: ["Atmel Corporation"]
sites: ["https://web.archive.org/web/*/www.atmel.com"]
country: usa
post: Microchip
---

* Founded in 1984, Atmel is an acronym for "Advanced Technology for Memory and Logic".
* In 1993 acquired the technology and certain technical employees of Concurrent Logic, to incorporta FPGAs to its portfolio.
* The AT6000 FPGAs (former CLi6000 from Concurrent) were first shipped in 1994. It was a fine-grain SRAM-based manufactured in 800 nm. In 1996 a Low Voltage version (AT6000LV) was announced. They were targeted as ideal for use as reconfigurable coprocessors and implement compute intensive logic.
* In 1999, a second generation, AT40K and AT40KLV were launched, in 600 nm.
* 2002 was the year of the AT94K, know also as FPSLIC (Field Programmable System Level Integrated Circuit), a chip which combines one AT40K and one AVR processor (RISC, 8 bits) and peripherals. It had variants of 3.3 and 1.8V, and also a Secure Configuration version, which included an EEPROM.
* In 1998, Atmel and Honeywell announced the co-development of a radiation-hardened FPGA, based on the AT6010.
* Probably as consequence of the co-design, Atmel launched in 2003 a Rad Hard version of AT40K, using 350 nm. ATF280E and ATF280F were lauched in 2007 and 2012, using 180 nm.
* The ATF280F was used in Multi-Chip Modules (MCM): ATFEE560 (2 x ATF280 + 2 x EEPROM) and ATF697 (ATF280 + a Sparc V8, 32 bits processor).
