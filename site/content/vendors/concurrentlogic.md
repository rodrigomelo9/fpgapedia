---
title: "Concurrent Logic"
years: [1985, 1993]
names: ["Concurrent Logic, Inc"]
country: usa
post: Atmel
---

* Was founded in 1985, with the vision that reconfigurable hardware could accelerate a wide range of compute-intensive tasks.
* Between 1987 and 1990 there was a joint development with Apple, presented in a paper as Labyrinth, a fine-grain SRAM-based FPGA architecture. In this work, a 64 cell test chip was built and tested (1600 nm), and a 4096 cell chip was in preparation for fabrication (1200 nm). Also, there are related patents asigned to both companies, between 1989 and 1992.
* There is an Advanced Info datasheet from Concurrent Logic, for the FPGA called CFA6006 (March of 1991).
* In 1992 Concurrent presented the CLi6000 Series of fine-grain SRAM-based FPGAs (800 nm). It seems that only CLi6002 and CLi6005 were commercialy available.
* CLi6000 is a new architecture, not based on Labyrinth.
* Also in 1992, Concurrent Logic licensed the CLI6000 to National Semiconductor, who call CLAy to its FPGAs.
* In 1993 Atmel acquired Concurrent Logic, and the CLi6000 family was renamed to AT6000.

* F. Furtek. G. Stone and I. W. Jones, "Labyrinth: A Homogeneous Computational Medium", Proceedings of the IEEE Custom Integrated Circuits Conference. May 1990.
* Concurrent Logic, Inc. Data Sheet CFA6006 Field Programmable Gate Array; Advanced Info.; Mar. 15, 1991, Rev. 2.2.
* Concurrent Logic, Inc., "CLi6000 Series Field Programmable Gate Array," Rev. 1.3, May 1992.
