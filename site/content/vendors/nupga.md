---
title: NuPGA
years: [2009, 2011]
names: ["NuPGA Corporation"]
sites: ["https://web.archive.org/web/20100901030159/http://nupga.com/"]
country: usa
---

* Founded in 2009, focused on the development of PLDs with density, power and performance similar to an ASICs.
* They specialized in architectures and implementation of 3D integrated circuits, using a carbon-based memory process. Graphite was considerated as antifuse reprogrammable memory element.
* In 2011, NuPGA changed its name to MonolithIC 3D Inc, because they discovered a path for practical monolithic 3D ICs, and changed the company focus.
* They never comercialized an FPGA.

Startup NuPGA was founded by Zvi Or-Bach, a winner of the EE Times Innovator of the Year Award. He previously founded eASIC and Chip Express.
