---
title: Motorola
years: [1992, 1998]
names: ["Motorola Inc"]
sites: ["https://web.archive.org/web/*/www.design-net.com/fpga/fpga.html"]
country: usa
prev: Pilkington
---

* Motorola licensed the SRAM-based architecture of Pilkington in 1992. Its first FPGAs were announced in 1995 and launched in 1996, as the MPA (Motorola Programmable Array) 1000 Series, manufactured (according the technical product description) in 430 nm.
* In 1997 Motorola announced the acquisition of Pilkington, which is absorved and renamed as Motorola Programmable Technology Center.
* One year later, Motorola announced that is exiting from the FPGA market, because a major restructuration (with FPGAs business in a early stages).
