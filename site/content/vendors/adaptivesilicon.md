---
title: "Adaptive Silicon"
years: [1999, 2002]
names: ["Adaptive Silicon, Inc"]
sites: ["https://web.archive.org/web/*/www.adaptivesilicon.com"]
country: usa
---

* ASi was founded in 1999.
* In 2001 they announced the Multi-Scale Array (MSA 2500) architecture. Its basic blocks could be used as 4 input ALUs or 3 inputs LUTs.
* LSI Logic Corp., an investor in the startup since 1999, has licensed the MSA architecture, and it has produced test chips using its 180 nm technology. The licensed core was called LiquidLogic and announced a short time as advanced product in the LSI's website (2001), but seems that was not offered anymore.
* ASi was acquired by Stretch in 2002, who developed SoCs with eFPGA.

* www.adaptivesilicon.com (Mar, 2002)
* www.lsilogic.com/products/asic/advanced_products.html (Oct, 2001)
* Reprogrammable Processing Capabilities of Embedded FPGA Blocks, Theodore Vaida, LSI Logic Corp. 2001.
* www.stretchinc.com (Jul, 2008)
