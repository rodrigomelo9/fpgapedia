---
title: Honeywell
years: [1998, 2007]
names: ["Honeywell international"]
country: usa
---

* In 1998, Atmel and Honeywell announced the co-development of a radiation-hardened FPGA, based on the AT6010.
* A paper (2004) talk about RhrFPGA.
* There is a presentation by Honeywell, where are mentions about HXFPGA6010 (introduced in 2001), but no more information was found.

* Advanced ASICs For Obsolete FPGA Replacement, Honeywell International. 2007.
