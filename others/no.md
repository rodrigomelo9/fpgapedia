# No FPGA vendors

## Ambric

* Ambric Inc.
* Founded in 2003.
* In 2006 introduced the AM2000 family of Massively Parallel Processor Arrays (MPPAs).
* Several places speak about Ambric as FPGA start-up, but its product is in fact an array of hundreds of 32-bit RISC processors and distributed memories.
* Out of business in 2008.
* Chips and tool were acquired by Nethra Imaging, Inc.

* www.ambric.com (May, 2008)

## eASIC

* Founded in 1999.
* Their products are in some place between ASICs and FPGAs.
* The programmable logic part is LUT3-based SRAM-based, but the routing is based in metalic connections.

* www.easic.com (Jun, 2018)

## MathStar

* MathStar Inc. Founded in 1997.
* they developed Field Programmable Object Arrays (FPOAs). Two generations: Builder (2004) and Arrix (2007).
* Several places speak about MathStar as FPGA start-up, but its products are in fact an array of Arithmetic Logic Units (ALUs), Multiply Accumulators (MACs) and registers. Is more like an array of DSPs.
* Defunct in 2010. Acquired by Sajan.

* www.mathstar.com (Jun, 2008)
* www.mathstar.com/FAQ2.php (Dec, 2008) -> What is the difference between an FPGA and an FPOA?

## Plus Logic

* Plus Logic Inc. Founded at late 1987.
* They argued that its products, called FPGA2010/20/40 (FPGA 2000 family) were PLA-based EPROM-based FPGAs (1200 nm). However, their architecture was more like a CPLD. Also, in a paper, is compared with the Altera MAX and AMD Mach families, which were CPLDs.
* Other products, which seems not marketed, were the FPSL5110/5210 (Field Programmable Subsystem Logic Family), both of them based on a FPGA2010. The FPSL5110 can be used to implement dual port memory and the FPSL5210 to implement large FSMs.
* In early 1992, Xilinx acquired Plus Logic, to became its CPLD Division. The XC7236 and XC7272 were a design revision of the FPGA 2010 and 2020.

* FAST PARTITIONING METHOD FOR PLA-BASED ARCHITECTURES, Zafar Hasan, Dave Harrison, Maciej J. Ciesielsh. ASIC Conference and Exhibit, 1992.
* Plus Logic FPGA2020 Preliminary Data Sheet, 1990
* Plus Logic, FPSL5110 Product Brief , October 1989
* FPGA 2010, Field Programmable Gate Array, Plus Logic, San Jose, Calif., no date, pp. 1-4.
* FPGA 2020, Field Programmable Gate Array, Plus Logic, San Jose, Calif., no date, pp. 1-37.
* FPGA 2040, Field Programmable Gate Array, Plus Logic, San Jose, Calif., no date, pp. 1-4.
* FPGA 5110, Intelligent Data Buffer, Plus Logic, San Jose, Calif., no date, pp. 1-6.

## Samsung

* Is menciones in the article "Wanted: FPGA start-up! …Dead or Alive?" (2011) from SemiWiki-com. Not more related info was found.

## Signetics

* Signetics (Philips).
* Has been some products (such as PLS103 and PLS151, 1987-1989) which were called FPGA, but were in fact something more simple than a PLA (only one level of ANDs/NANDs, selectable by a XOR).
* A paper was presented in 1990 about a User Configurable Gate Array. It has not the tipical matricial architecture of a FPGA. Is a PLS.

* A USER CONFIGURABLE GATE ARRAY USING CMOS-EPROM TECHNOLOGY. Ani Gupta, Vinita Aggarwal, Rakesh Patel, Prasad Chalasani, Dante Chu, Paramesh Seeni, Pin-Wu Liu, John Wu, Gerrit Kaat. Custom Integrated Circuits Conference, 1990.
