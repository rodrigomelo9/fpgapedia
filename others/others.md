# BTR

* BTR Inc (USA).
* Related persons: Benjamin S. Ting, Peter M. Pani.
* BTR Inc has patents related to FPGA technology, which were filled between 1993 and 2006 and are nowadays asigned to Actel or Microsemi.
* There is a License Agreement between BTR Inc. and Actel Corp. in 1995, where BTR licensed its FPGA and Multichip Modules technologies to Actel.
* The BTR technology was used in the ES family (embeded SPGA) of Actel: there is a paper in 1999 about Actel's RS family (SRAM-based) where one of the authors is affiliated to BTR Inc and figured as inventor in the patents.
* After demands about patents, Actel acquired patents and other intellectual property assets from BTR Inc and Advantage Logic in 2007. See ASSET PURCHASE AGREEMENT between Actel and BTR/ALI.
* Some patents also appears as asigned to Advantage Logic, with the same inventors.
* Advantage Logic, was a company in the same place that BTR (Nevada, 20380 Town Center Lane, Suite 250, Cupertino, CA 95014) with the same related persons. It seems another name of the same company, maybe to try to avoid problems about patents.

Papers:
* A new high density and very low cost reprogrammable FPGA architecture, 1999, Sinan Kaptanoglu and Greg Bakker and Arun Kundu and Ivan Corneillet (Actel), Ben Ting (BTR).

BTR Patents:
* Architecture and interconnect scheme for programmable logic circuits, 1993, Benjamin S. Ting, BTR Inc (Actel Corp).
* Apparatus and method for partitioning resources for interconnections, 1994, Benjamin S. Ting, BTR Inc (Actel Corp).
* Scalable multiple level tab oriented interconnect architecture, 1995, Benjamin S. Ting and Peter M. Pani, BTR Inc (Actel Corp).
* Programmable non-volatile bidirectional switch for programmable logic, 1997, Peter M. Pani and Benjamin S. Ting and Benny Ma, BTR Inc (Microsemi Corp).

Advantage Logic Patents:
* Architecture and interconnect scheme for programmable logic circuits, 1993, Benjamin S. Ting, Advantage Logic Inc.
* Apparatus and method for partitioning resources for interconnections, 1994, Benjamin S. Ting, Advantage Logic Inc.
* Method and apparatus for universal program controlled bus architecture, 1996, Benjamin S. Ting and Peter M. Pani, Advantage Logic Inc (Microsemi Corp).
* Scalable non-blocking switching network for programmable logic, 2004, Peter M. Pani and Benjamin S. Ting, Advantage Logic Inc (RPX Corp).
* Efficient integrated circuit layout scheme to implement a scalable switching network used in interconnection fabric, 2006, Benjamin S. Ting
and Peter M. Pani, Advantage Logic Inc (RPX Corp).
* Interconnection fabric using switching networks in hierarchy, 2004, Peter M. Pani and Benjamin S. Ting, Advantage Logic Inc (RPX Corp).
* Enhanced scheme to implement an interconnection fabric using switching networks in hierarchy, 2006, Peter M. Pani and Benjamin S. Ting, Advantage Logic Inc (RPX Corp).
* Permutable switching network with enhanced interconnectivity for multicasting signals, 2008, Peter M. Pani and Benjamin S. Ting, Advantage Logic Inc (RPX Corp).
* Enhanced permutable switching network with multicasting signals for interconnection fabric, 2009, Peter M. Pani and Benjamin S. Ting, Advantage Logic Inc (RPX Corp).
* Modular routing fabric using switching networks, 2009, Peter M. Pani and Benjamin S. Ting, Advantage Logic Inc (RPX Corp).

Notes:
* RPX Corporation (Rational Patent EXchange) was an American worldwide provider of patent risk management services, which offered defensive buying and other services.
* Advantage Logic became BTR to move to Nevada for tax purposes.

# Hewlett Packard

* Hewlett Packard (USA)
* In 1994, HP and Intel announced the being of the joint development of the first 64-bit Intel processor. They decided to also produce its own Configurable Custom Computing to be used as emulator, which was presented in 1995, and was called Teramac (Tera Multiple Architecture Computer).
* Commercial FPGAs were considerated, but none of them met they needs, related to Place and Route time, the unknown propietary format of the bitstreams and the cost of license or the development of tools.
* Instead, they used an HP custom-designed FPGA called Plasma (Programmable Logic And Switch Matrix), using LUTs and SRAM-based, was poduced in 800 nm, and packaged in large Multichip Modules (MCMs). Its architecture simplified the compiler, and involved times.
* The FPGA developed by the Research Laboratories of HP was only used for the Teramac (not commercialized).

* Plasma: An FPGA for Million Gate Systems, R. Amerson, R. Carter, W. Culbertson, P. Kuekes, G. Snider. 1996.

## LSI Logic

* ASICs.
* Kawasaki Steel Corporation (Japan)
* In 1990, the LSI Division of Kawasaki Steel described a LUT-based SRAM-based FPGA.
* A prototype chip was fabricated (1000 nm), which included memory to be used as RAM or FIFO.
* No more related work was found.

* K. Kawana, H. Keida, M. Sakamoto, K. Shibata, and I. Moriyama, "An effcient logic block interconnect architecture for user-reprogrammable gate array," in Proceedings of the IEEE 1990 Custom Integrated Circuits Conference , pp. 31.3.1{31.3.4, May 1990.

# ProSys

* ProSys Technology Inc (USA).
* In 1996, Actel planned to market an SRAM-based FPGA family that optionally contained ASIC-housed logic and memory cores. These products never materialized, but several of the architects involved in the development, left Actel to form Prosys.
* In 2000, Prosys was acquired again by Actel, to use the developed technology in VariCore eFPGA.
* No much more information about ProSys was found.

* "SRAM-based FPGAs cut to the core", www.edn.com (June 22, 2000).

# STMicroelectronics

* Italy-France.
* Was created as SGS-THOMSON Microelectronics in 1987, from merger of SGS Microelettronica (Italy) and Thomson Semiconducteurs (France). Renamed STMicroelectronics in 1998.
* In 1995, ST worked with Oxford University on an FPGA architecture, but that effort never produced a prototype.
* In year 2000 there were news about ST working in own FPGA and related tools.
* The GOSPL (Generalized Open Source Programmable Logic) was presented in 2004, and stoped by ST in 2005. This year a MCU with a licensed eFPGA from M2000 was announced.

* The Design of a New FPGA Architecture, Anthony Stanseld and Ian Page. 1995.
* www.gospl.org/fpl/static/aboutgospl.jsp (Dec, 2004)
