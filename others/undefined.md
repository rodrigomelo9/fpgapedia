# GTE Microelectronics

In the 1996 edition of the document *"North American Company Profiles"* offered by Integrated Circuit Engineering Corporation (ICE), you can read: *"GTE Microelectronic's products include ASICs, FPGAs, multichip modules, and hybrids."*.
In the next year version, is said that the company was acquired by Micro Networks Corporation, but nothing more was found about FPGAs from GTE or Micro Networks.

# Prizm

Dr Stephen Trimberger, who worked at Xilinx since 1988 to 2017, wrote *Three Ages of FPGAs: A Retrospective on the First Thirty Years of FPGA Technology* (IEEE, vol. 103, no. 3, pp. 318-331, March 2015). Not there, but in the associated presentation, he spoke about mass extinctions of architectures in the mid of the 90s: *"XC8100, XC6200, XC4700, Prizm, ... Plessey, Toshiba, Motorola, IBM..."*.
In *FPGA-based Implementation of Signal Processing Systems*, and also in other books, the following text was found: *"At this stage, the FPGA market was populated by a number of vendors, including Xilinx, Altera, Actel, Lattice, Crosspoint, Prizm, Plessey, Toshiba, Motorola, Algotronix and IBM..."*. 
However, after that, a reference to a plenary talk of Dr Trimberger in the FPL2007 was found.
There is a summary of this talk, with a short biography about Stephen, where can be read: *"He was the technical leader for the Xilinx XC4000 design automation software, designed the Prizm dynamically-reconfigurable FPGA, led the architecture definition group for the Xilinx XC4000X device families and designed the Xilinx bitstream security functions in the Virtex families of FPGAs."*

I guess that Prizm was the codename of one Xilinx architecture, which was misunderstood to be an FPGA vendor and the "copy and paste" made the rest.
