# FPGApedia: the FPGA encyclopedia

Welcome to the FPGApedia project, a place mainly dedicated to collect information about FPGA devices, its vendors and related companies.

Visit [FPGApedia](https://rodrigomelo9.gitlab.io/fpgapedia).

Something is wrong or missing? There is any mistake in a technical or historical fact? The text is bad or poorly written in English (take into account that my mother tongue is Spanish)? Please, feel free to open an [issue](https://gitlab.com/rodrigomelo9/fpgapedia/issues) or contact me. Any feedback or contribution will be welcome.

![Creative Commons Attribution 4.0 International Logo](https://i.creativecommons.org/l/by/4.0/80x15.png)
This work is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).

## FPGA vendors

[![Achronix](images/buttons/Achronix.png)](vendors/Achronix.md)
[![Actel](images/buttons/Actel.png)](vendors/Actel.md)
[![Aeroflex](images/buttons/Aeroflex.png)](vendors/Aeroflex.md)
[![Agate / CME](images/buttons/Agate-CME.png)](vendors/Agate-CME.md)
[![AGM](images/buttons/AGM.png)](vendors/AGM.md)
[![Algotronix](images/buttons/Algotronix.png)](vendors/Algotronix.md)
[![Altera](images/buttons/Altera.png)](vendors/Altera.md)
[![Anlogic](images/buttons/Anlogic.png)](vendors/Anlogic.md)
[![Atmel](images/buttons/Atmel.png)](vendors/Atmel.md)
[![ATT / Lucent / Agere](images/buttons/ATT-Lucent-Agere.png)](vendors/ATT-Lucent-Agere.md)
[![Cobham](images/buttons/Cobham.png)](vendors/Cobham.md)
[![ConcurrentLogic](images/buttons/ConcurrentLogic.png)](vendors/ConcurrentLogic.md)
[![Crosspoint](images/buttons/Crosspoint.png)](vendors/Crosspoint.md)
[![CSMT](images/buttons/CSMT.png)](vendors/CSMT.md)
[![CSwitch](images/buttons/CSwitch.png)](vendors/CSwitch.md)
[![Cypress](images/buttons/Cypress.png)](vendors/Cypress.md)
[![DynaChip](images/buttons/DynaChip.png)](vendors/DynaChip.md)
[![Efinix](images/buttons/Efinix.png)](vendors/Efinix.md)
[![Flexlogics / Velogix](images/buttons/Flexlogics-Velogix.png)](vendors/Flexlogics-Velogix.md)
[![Gatefield](images/buttons/Gatefield.png)](vendors/Gatefield.md)
[![Gowin](images/buttons/Gowin.png)](vendors/Gowin.md)
[![IBM](images/buttons/IBM.png)](vendors/IBM.md)
[![Intel](images/buttons/Intel.png)](vendors/Intel.md)
[![Lattice](images/buttons/Lattice.png)](vendors/Lattice.md)
[![M2000 / Abound](images/buttons/M2000-Abound.png)](vendors/M2000-Abound.md)
[![Microchip](images/buttons/Microchip.png)](vendors/Microchip.md)
[![Microsemi](images/buttons/Microsemi.png)](vendors/Microsemi.md)
[![MMI / AMD / Vantis](images/buttons/MMI-AMD-Vantis.png)](vendors/MMI-AMD-Vantis.md)
[![Motorola](images/buttons/Motorola.png)](vendors/Motorola.md)
[![MRC](images/buttons/MRC.png)](vendors/MRC.md)
[![National](images/buttons/National.png)](vendors/National.md)
[![NEC](images/buttons/NEC.png)](vendors/NEC.md)
[![NuPGA](images/buttons/NuPGA.png)](vendors/NuPGA.md)
[![Pango](images/buttons/Pango.png)](vendors/Pango.md)
[![Pilkington](images/buttons/Pilkington.png)](vendors/Pilkington.md)
[![Plessey](images/buttons/Plessey.png)](vendors/Plessey.md)
[![Quicklogic](images/buttons/Quicklogic.png)](vendors/Quicklogic.md)
[![SiliconBlue](images/buttons/SiliconBlue.png)](vendors/SiliconBlue.md)
[![Tabula](images/buttons/Tabula.png)](vendors/Tabula.md)
[![TI](images/buttons/TI.png)](vendors/TI.md)
[![Toshiba](images/buttons/Toshiba.png)](vendors/Toshiba.md)
[![Triscend](images/buttons/Triscend.png)](vendors/Triscend.md)
[![Viciciv / TierLogic](images/buttons/Viciciv-TierLogic.png)](vendors/Viciciv-TierLogic.md)
[![Xilinx](images/buttons/Xilinx.png)](vendors/Xilinx.md)
[![XiST](images/buttons/XiST.png)](vendors/XiST.md)

Notes:
* Crosspoint logo was not found.
* Apparently Viciciv did not have a logo.

# eFPGA providers

[![Achronix](images/buttons/Achronix.png)](vendors/Achronix.md)
[![ADICSYS](images/buttons/ADICSYS.png)](others/efpga.md#adicsys)
[![Adaptive Silicon](images/buttons/AdaptiveSilicon.png)](others/efpga.md#adaptive-silicon)
[![Flex Logix](images/buttons/FlexLogix.png)](others/efpga.md#flex-logix)
[![Leopard Logic](images/buttons/LeopardLogic.png)](others/efpga.md#leopard-logic)
[![Menta](images/buttons/Menta.png)](others/efpga.md#menta)
[![NanoXplore](images/buttons/NanoXplore.png)](others/efpga.md#nanoxplore)
[![Quicklogic](images/buttons/Quicklogic.png)](vendors/Quicklogic.md)

## Other related companies

[BTR](others/others.md#btr) -
[HP](others/others.md#hewlett-packard) -
[Honeywell](others/others.md#honeywell) -
[LSI](others/others.md#lsi-logic) -
[Peregrine](others/others.md#peregrine) -
[ProSys](others/others.md#prosys) -
[SEi](others/others.md#sei) -
[SIDSA](others/others.md#sidsa) -
[ST](others/others.md#stmicroelectronics)

## No FPGA vendors

[Ambric](others/no.md#ambric) -
[eASIC](others/no.md#easic) -
[MathStar](others/no.md#mathstar) -
[Plus Logic](others/no.md#plus-logic) -
[Samsung](others/no.md#samsung) -
[Signetics](others/no.md#signetics)

## Undefined FPGA vendors

[GTE Microelectronics](others/undefined.md#gte-microelectronics) -
[Prizm](others/undefined.md#prizm)
